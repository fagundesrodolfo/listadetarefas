package com.fagundes.rodolfo.listaDeTarefas

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.fagundes.rodolfo.listaDeTarefas.model.Banco
import com.fagundes.rodolfo.listaDeTarefas.model.Item
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class ViewModelListItens : ViewModel() {

    var itemSelected: Item? = null
    val listItens: MutableList<Item> = arrayListOf()

    /**
     * Inicia a lista buscando os dados salvos no banco de dados
     * */
    fun startList(context: Context): MutableLiveData<MutableList<Item>> {
        val liveData = MutableLiveData<MutableList<Item>>()

        GlobalScope.launch {
            val temp = Banco.getDataBase(context).DAOItem().findAll()
            if (temp.isNotEmpty()) {
                listItens.clear()
                listItens.addAll(temp)
            }
            liveData.postValue(listItens)
        }
        return liveData

    }

    fun saveItem(context: Context, item: Item): MutableLiveData<Item> {
        val liveData = MutableLiveData<Item>()
        GlobalScope.launch {
            val id = if (item.id > 0L) {
                Banco.getDataBase(context).DAOItem().update(item).toLong()
            } else {
                Banco.getDataBase(context).DAOItem().save(item)
            }
            if (item.id == 0L) {
                item.id = id
                listItens.add(item)
            }

            liveData.postValue(item)
        }
        return liveData
    }

    fun deleteItem(context: Context, item: Item): MutableLiveData<Int> {
        val liveData = MutableLiveData<Int>()
        GlobalScope.launch {
            val id = Banco.getDataBase(context).DAOItem().delete(item)
            listItens.remove(item)
            liveData.postValue(id)
        }
        return liveData
    }

    fun ordenar() {
        listItens.sortBy { it.nome }
    }

    fun exibirSelecionados() {
        val temp = listItens.filter { it.selecionado }
        listItens.clear()
        listItens.addAll(temp)
    }

}