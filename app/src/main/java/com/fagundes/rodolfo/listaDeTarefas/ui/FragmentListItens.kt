package com.fagundes.rodolfo.listaDeTarefas.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.fagundes.rodolfo.listaDeTarefas.R
import com.fagundes.rodolfo.listaDeTarefas.ViewModelListItens
import com.fagundes.rodolfo.listaDeTarefas.model.Item
import kotlinx.android.synthetic.main.fragment_list_itens.view.*


class FragmentListItens : Fragment(),
    AdapterListItens.CallBackItens {

    private val viewModel: ViewModelListItens by activityViewModels()

    private lateinit var root: View

    private val recyclerListItens by lazy { root.reciclerListItens }
    private val textViewitensSelecionados by lazy { root.textViewitensSelecionados }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        root = inflater.inflate(R.layout.fragment_list_itens, container, false)

        root.fab.setOnClickListener {
            navigateToFragmentEditItem(null)
        }

        viewModel.startList(requireContext()).observe(viewLifecycleOwner,
            Observer { list ->
                startRecyclerView(list)
                atualizaContagemDeItens()
            })

        return root
    }

    private fun startRecyclerView(list: MutableList<Item>) {
        recyclerListItens.apply {
            layoutManager = LinearLayoutManager(requireContext())
            adapter = AdapterListItens(
                list,
                this@FragmentListItens
            )
        }
    }

    override fun editItem(item: Item) {
        navigateToFragmentEditItem(item)
    }

    override fun removeItem(item: Item, position: Int) {

        viewModel.deleteItem(requireContext(), item)
            .observe(viewLifecycleOwner,
                Observer {
                    if (it > 0) {
                        recyclerListItens.adapter?.notifyItemRemoved(position)
                    }
                })

    }

    override fun itemSelected(item: Item) {
        viewModel.saveItem(requireContext(), item)
        atualizaContagemDeItens()
    }

    private fun atualizaContagemDeItens() {
        val total = viewModel.listItens.size
        val contItensSelecionados = viewModel.listItens.filter { it.selecionado }.size

        val text = " $total / $contItensSelecionados"

        textViewitensSelecionados.text = text
    }


    fun navigateToFragmentEditItem(item: Item?) {
        viewModel.itemSelected = item
        findNavController().navigate(R.id.action_FirstFragment_to_fragmentItemEdit)
    }

}