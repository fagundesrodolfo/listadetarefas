package com.fagundes.rodolfo.listaDeTarefas.ui

import android.app.DatePickerDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.addCallback
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.fagundes.rodolfo.listaDeTarefas.R
import com.fagundes.rodolfo.listaDeTarefas.ViewModelListItens
import com.fagundes.rodolfo.listaDeTarefas.model.Item
import kotlinx.android.synthetic.main.fragment_item_edit.view.*
import java.text.SimpleDateFormat
import java.util.*


class FragmentItemEdit : Fragment() {

    private val viewModel: ViewModelListItens by activityViewModels()

    private lateinit var root: View
    private lateinit var item: Item

    private val editNome by lazy { root.editNome }
    private val editDescricao by lazy { root.editDescricao }

    private val calendar by lazy { builderCalendar() }

    private fun builderCalendar(): Calendar {
        val calendar = Calendar.getInstance()
        calendar.timeInMillis = System.currentTimeMillis()
        return calendar
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        root = inflater.inflate(R.layout.fragment_item_edit, container, false)

        root.salvarButton.setOnClickListener {
            salvaItem()
        }

        root.cancelarButton.setOnClickListener {
            alertBoBack()
        }

        item = viewModel.itemSelected ?: Item()

        if (item.nome.isNotEmpty()) {
            editNome.setText(item.nome)
        }

        if (item.descricao.isNotEmpty()) {
            editDescricao.setText(item.descricao)
        }

        requireActivity().onBackPressedDispatcher.addCallback(this.viewLifecycleOwner) {
            alertBoBack()
        }

        root.buttonData.setOnClickListener {
            DatePickerDialog(
                requireContext(),
                DatePickerDialog.OnDateSetListener { _, year, month, dayOfMonth ->
                    calendar[Calendar.YEAR] = year
                    calendar[Calendar.MONTH] = month
                    calendar[Calendar.DAY_OF_MONTH] = dayOfMonth
                    setData()
                },
                calendar[Calendar.YEAR] ,
                calendar[Calendar.MONTH],
                calendar[Calendar.DAY_OF_MONTH]
            ).show()
        }

        setData()

        return root
    }

    private fun setData(){
        val simpleDateFormat = SimpleDateFormat("dd/MM/yyyy",Locale.getDefault())
        val date = simpleDateFormat.format(Date(item.data))
        root.editData.setText(date)
    }

    private fun alertBoBack() {
        val dialog: androidx.appcompat.app.AlertDialog
        val builder = androidx.appcompat.app.AlertDialog.Builder(requireContext())

        builder.setMessage(R.string.desejaCancelar)
        builder.setPositiveButton(R.string.yes) { _, _ ->
            navigateToFragmentList()
        }
        builder.setNegativeButton(R.string.no) { _, _ ->

        }
        dialog = builder.create()
        dialog.show()
    }

    private fun salvaItem() {

        if (editNome.text.isNullOrEmpty()) {
            root.nameItem.error = "Digite um nome"
            return
        } else {
            root.nameItem.error = ""
            root.nameItem.isErrorEnabled = false
            item.nome = editNome.text.toString()
        }

        item.descricao = editDescricao.text.toString()

        item.data = calendar.timeInMillis

        viewModel.saveItem(requireContext(), item).observe(
            viewLifecycleOwner, Observer { itemTemp ->
                if (itemTemp.id != 0L) {
                    val dialog: androidx.appcompat.app.AlertDialog
                    val builder = androidx.appcompat.app.AlertDialog.Builder(requireContext())

                    builder.setMessage(R.string.itemSalvo)
                    builder.setPositiveButton(R.string.ok) { _, _ ->
                        navigateToFragmentList()
                    }

                    dialog = builder.create()
                    dialog.show()
                } else {
                    val dialog: androidx.appcompat.app.AlertDialog
                    val builder = androidx.appcompat.app.AlertDialog.Builder(requireContext())

                    builder.setMessage(R.string.erroAoSalvar)
                    builder.setPositiveButton(R.string.ok) { _, _ ->
                        navigateToFragmentList()
                    }

                    dialog = builder.create()
                    dialog.show()
                }
            }
        )

    }


    private fun navigateToFragmentList() {
        findNavController().navigate(R.id.action_fragmentItemEdit_to_FirstFragment)
    }


}