package com.fagundes.rodolfo.listaDeTarefas.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Item(
    @PrimaryKey(autoGenerate = true)
    var id: Long,
    var nome: String,
    var descricao: String,
    var selecionado: Boolean,
    var data: Long
) {
    constructor() : this(0L, "", "", false, System.currentTimeMillis())
}