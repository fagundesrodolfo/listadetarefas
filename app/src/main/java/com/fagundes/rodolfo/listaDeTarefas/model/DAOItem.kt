package com.fagundes.rodolfo.listaDeTarefas.model

import androidx.room.*

@Dao
interface DAOItem {

    @Query("SELECT * FROM Item")
    fun findAll(): List<Item>

    @Insert
    fun save(item: Item): Long

    @Update
    fun update(item: Item): Int

    @Delete
    fun delete(item: Item): Int

    @Query("SELECT * FROM Item ORDER BY Item.nome ASC;")
    fun ordemAlfabetica(): Item?

}

