package com.fagundes.rodolfo.listaDeTarefas.model

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase

@Database(
    entities = [Item::class],
    version = 2,
    exportSchema = false
)
abstract class Banco : RoomDatabase() {

    abstract fun DAOItem(): DAOItem



    companion object {

        val MIGRATION_1_2 = object : Migration(1, 2) {
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL("ALTER TABLE Item ADD COLUMN data INTEGER")
            }
        }

        fun getDataBase(context: Context): Banco {
            return Room
                .databaseBuilder(context, Banco::class.java, "lista_compras")
                .addMigrations(MIGRATION_1_2)
                .build()
        }

    }
}