package com.fagundes.rodolfo.listaDeTarefas.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.fagundes.rodolfo.listaDeTarefas.model.Item
import com.fagundes.rodolfo.listaDeTarefas.R
import kotlinx.android.synthetic.main.fragment_list_itens_item.view.*

class AdapterListItens(
    val listItens: MutableList<Item>,
    val callback: CallBackItens
) : RecyclerView.Adapter<AdapterListItens.Holder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.fragment_list_itens_item, parent, false)
        return Holder(
            view
        )
    }

    override fun getItemCount() = listItens.size

    override fun onBindViewHolder(holder: Holder, position: Int) {

        listItens[position].let { item ->
            holder.apply {
                checkboxItemList.text = item.nome
                checkboxItemList.isChecked = item.selecionado
                checkboxItemList.setOnCheckedChangeListener { _, b ->
                    item.selecionado = b
                    callback.itemSelected(item)
                }
                butonEditItem.setOnClickListener { callback.editItem(item) }
                butonRemoveItem.setOnClickListener { callback.removeItem(item, position) }
            }
        }
    }

    class Holder(view: View) : RecyclerView.ViewHolder(view) {
        val checkboxItemList by lazy { view.checkboxItemList }
        val butonEditItem by lazy { view.butonEditIten }
        val butonRemoveItem by lazy { view.butonRemoveIten }
    }

    interface CallBackItens {

        fun editItem(item: Item)

        fun removeItem(item: Item, position: Int)

        fun itemSelected(item: Item)

    }


}
